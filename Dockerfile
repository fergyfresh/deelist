FROM python:3.6-alpine

RUN pip install gunicorn

COPY web/requirements.txt /requirements.txt
RUN pip install -r requirements.txt

ADD web/* /app/
WORKDIR /app

CMD ["gunicorn"  , "-w", "8", "main:app"]
